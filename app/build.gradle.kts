plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = Dependency.Android.Build.compileSdkVersion
    defaultConfig {
        applicationId = Dependency.Android.Build.appId
        minSdk = Dependency.Android.Build.minSdkVersion
        targetSdk = Dependency.Android.Build.targetSdkVersion
        versionCode = Dependency.Android.Build.versionCode
        versionName = Dependency.Android.Build.versionName
        testInstrumentationRunner = Dependency.Android.Build.runner
    }

    buildTypes.getByName("debug") {
        isTestCoverageEnabled = true
        isDebuggable = true
    }

    buildTypes.getByName("release") {
        isTestCoverageEnabled = false
        isDebuggable = false
        isMinifyEnabled = false
        proguardFiles(
            getDefaultProguardFile("proguard-android-optimize.txt"),
            "proguard-rules.pro"
        )
    }

    buildFeatures {
        viewBinding = true
    }

    sourceSets {
        getByName("main").java.srcDir("src/main/kotlin")
        getByName("test").java.srcDir("src/test/kotlin")
        getByName("androidTest").java.srcDir("src/androidTest/kotlin")
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Dependency.Kotlin.stdLib)
    implementation(Dependency.Android.material)
    implementation(Dependency.Android.constraintLayout)
    implementation(Dependency.Android.coreKtx)
    implementation(Dependency.Android.viewModelKtx)
    implementation(Dependency.Android.activityKtx)
    implementation(Dependency.Android.fragmentKtx)
    implementation(Dependency.Android.gson)
    implementation(Dependency.Android.dataStore)

    implementation(Dependency.Kotlin.Coroutines.core)
    implementation(Dependency.Kotlin.Coroutines.android)

    implementation(Dependency.Android.hilt)
    kapt(Dependency.Android.hiltCompiler)

    testImplementation(Dependency.Test.coreTesting)
    testImplementation(Dependency.Test.mockito)
    testImplementation(Dependency.Test.jUnit4)
    testImplementation(Dependency.Test.turbine)
    testImplementation(Dependency.Test.coroutines) {
        exclude(group = "org.jetbrains.kotlinx", module = "kotlinx-coroutines-debug")
    }

    androidTestImplementation(Dependency.Test.turbine)
    androidTestImplementation(Dependency.Test.coroutines) {
        exclude(group = "org.jetbrains.kotlinx", module = "kotlinx-coroutines-debug")
    }
    androidTestImplementation(Dependency.Test.testRunner)
    androidTestImplementation(Dependency.Test.testExtension)
    androidTestImplementation(Dependency.Test.espresso)
    androidTestImplementation(Dependency.Test.espressoContrib)
}
