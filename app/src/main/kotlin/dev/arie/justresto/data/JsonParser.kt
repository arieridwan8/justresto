package dev.arie.justresto.data

import android.content.Context
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.io.StringWriter
import java.io.Writer
import javax.inject.Inject

const val CHARSET_NAME = "UTF-8"
const val BUFFER = 1024

class JsonParser @Inject constructor(
    private val context: Context
) {

    fun readRawJson(resId: Int): String {
        val inputStream: InputStream = context.resources.openRawResource(resId)
        val writer: Writer = StringWriter()
        val buffer = CharArray(BUFFER)

        inputStream.use { input ->
            val reader: Reader = BufferedReader(InputStreamReader(input, CHARSET_NAME))
            var counter: Int
            while (reader.read(buffer).also { counter = it } != -1) {
                writer.write(buffer, 0, counter)
            }
        }

        return writer.toString()
    }
}
