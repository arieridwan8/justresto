package dev.arie.justresto.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * Created by arieridwan on 16/12/21.
 */

const val JUST_RESTO_PREFERENCE = "justresto"
private const val JUST_RESTO_RESTAURANTS_KEY = "restaurants"

class PreferenceDataStoreManager @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    private val justRestoRestaurantsKey = stringPreferencesKey(JUST_RESTO_RESTAURANTS_KEY)

    suspend fun saveRestaurants(restaurants: String) {
        dataStore.edit { restaurantsPref ->
            restaurantsPref[justRestoRestaurantsKey] = restaurants
        }
    }

    fun getRestaurants(): Flow<String> = dataStore.data.map { preferences ->
        preferences[justRestoRestaurantsKey] ?: ""
    }
}
