package dev.arie.justresto.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dev.arie.justresto.data.JsonParser
import dev.arie.justresto.data.PreferenceDataStoreManager
import dev.arie.justresto.restaurant.RestaurantRepository
import dev.arie.justresto.restaurant.RestaurantRepositoryImpl
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    @ViewModelScoped
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @ViewModelScoped
    fun provideJsonParser(
        @ApplicationContext appContext: Context
    ) = JsonParser(appContext)

    @Provides
    @ViewModelScoped
    fun provideDispatcher() = Dispatchers.IO

    @Provides
    @ViewModelScoped
    fun provideRestaurantRepository(
        gson: Gson,
        jsonParser: JsonParser,
        dispatcher: CoroutineDispatcher,
        preferenceDataStoreManager: PreferenceDataStoreManager
    ): RestaurantRepository {
        return RestaurantRepositoryImpl(
            gson,
            jsonParser,
            dispatcher,
            preferenceDataStoreManager
        )
    }
}
