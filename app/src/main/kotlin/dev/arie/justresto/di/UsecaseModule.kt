package dev.arie.justresto.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import dev.arie.justresto.restaurant.RestaurantRepository
import dev.arie.justresto.restaurant.RestaurantUsecase
import dev.arie.justresto.restaurant.RestaurantUsecaseImpl
import kotlinx.coroutines.CoroutineDispatcher

/**
 * Created by arieridwan on 18/12/21.
 */
@Module
@InstallIn(ViewModelComponent::class)
object UsecaseModule {

    @Provides
    @ViewModelScoped
    fun provideUsecase(
        repo: RestaurantRepository,
        dispatcher: CoroutineDispatcher
    ): RestaurantUsecase {
        return RestaurantUsecaseImpl(
            repo,
            dispatcher
        )
    }
}
