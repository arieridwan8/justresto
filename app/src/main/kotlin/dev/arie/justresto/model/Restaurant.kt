package dev.arie.justresto.model

import com.google.gson.annotations.SerializedName

const val STATUS_OPEN = "open"
const val STATUS_ORDER_AHEAD = "order ahead"
const val STATUS_CLOSED = "closed"

data class Restaurant(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("sortingValues")
    val sortingValues: SortingValues,
    @SerializedName("status")
    val status: String
) {
    fun getSortingPriorityByOpeningHours() = when (status) {
        STATUS_OPEN -> 1
        STATUS_ORDER_AHEAD -> 2
        STATUS_CLOSED -> 3
        else -> 3
    }

    fun getSortValue(sortType: SortType): Double {
        return when(sortType) {
            SortType.BestMatch -> sortingValues.bestMatch
            SortType.Newest -> sortingValues.newest
            SortType.RatingAverage -> sortingValues.ratingAverage
            SortType.Distance -> sortingValues.distance.toDouble()
            SortType.Popularity -> sortingValues.popularity
            SortType.AverageProductPrice -> sortingValues.averageProductPrice.toDouble()
            SortType.DeliveryCost -> sortingValues.deliveryCosts.toDouble()
            SortType.MinimumCost -> sortingValues.minCost.toDouble()
        }
    }
}
