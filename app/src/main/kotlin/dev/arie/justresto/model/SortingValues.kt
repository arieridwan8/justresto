package dev.arie.justresto.model

import com.google.gson.annotations.SerializedName

data class SortingValues(
    @SerializedName("averageProductPrice")
    val averageProductPrice: Int,
    @SerializedName("bestMatch")
    val bestMatch: Double,
    @SerializedName("deliveryCosts")
    val deliveryCosts: Int,
    @SerializedName("distance")
    val distance: Int,
    @SerializedName("minCost")
    val minCost: Int,
    @SerializedName("newest")
    val newest: Double,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("ratingAverage")
    val ratingAverage: Double
)
