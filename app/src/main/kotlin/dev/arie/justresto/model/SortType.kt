package dev.arie.justresto.model

enum class SortType {
    BestMatch,
    Newest,
    RatingAverage,
    Distance,
    Popularity,
    AverageProductPrice,
    DeliveryCost,
    MinimumCost
}

enum class SortOrder {
    Ascending,
    Descending,
}
