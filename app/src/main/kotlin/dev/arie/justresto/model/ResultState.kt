package dev.arie.justresto.model

/**
 * Created by arieridwan on 17/12/21.
 */
sealed class ResultState<out T> {
    data class Success<out T>(val data: T) : ResultState<T>()

    data class Error<out T>(
        val message: String?,
        val cause: Throwable? = null,
    ) : ResultState<T>()

    object Loading : ResultState<Nothing>()
}
