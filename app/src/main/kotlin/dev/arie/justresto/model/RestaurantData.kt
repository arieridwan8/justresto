package dev.arie.justresto.model

import com.google.gson.annotations.SerializedName

data class RestaurantData(
    @SerializedName("restaurants")
    val restaurants: List<Restaurant>,
    val sortType: SortType = SortType.Distance,
    val sortOrder: SortOrder = SortOrder.Ascending
)
