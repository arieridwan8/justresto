package dev.arie.justresto

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by arieridwan on 16/12/21.
 */

@HiltAndroidApp
class JustRestoApp: Application()
