package dev.arie.justresto.restaurant

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import dev.arie.justresto.R
import dev.arie.justresto.databinding.ActivityRestaurantBinding

@AndroidEntryPoint
class RestaurantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRestaurantBinding
    private val restaurantAdapter = RestaurantAdapter()
    private val restaurantViewModel: RestaurantViewModel by viewModels()
    private var sortRestaurantDialogFragment: SortRestaurantDialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRestaurantBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
        observerViewEvents()
        restaurantViewModel.loadRestaurants()
    }

    private fun initViews() {
        binding.rvRestaurant.adapter = restaurantAdapter
        binding.rvRestaurant.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 10 && binding.fabSort.isShown) {
                    binding.fabSort.hide()
                }

                if (dy < -10 && !binding.fabSort.isShown) {
                    binding.fabSort.show()
                }

                if (!recyclerView.canScrollVertically(-1)) {
                    binding.fabSort.show()
                }
            }
        })
        binding.fabSort.setOnClickListener {
            sortRestaurantDialogFragment?.dismiss()
            sortRestaurantDialogFragment = SortRestaurantDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(
                        SEARCH_KEYWORD,
                        binding.etSearch.text.toString()
                    )
                }
                show(supportFragmentManager, tag)
            }
        }
        binding.etSearch.addTextChangedListener {
            restaurantViewModel.searchRestaurantOnTyping(it.toString())
            binding.ivClear.isVisible = it.toString().isNotEmpty()
        }
        binding.etSearch.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                restaurantViewModel.searchRestaurant(textView.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        binding.ivClear.setOnClickListener {
            binding.etSearch.text?.clear()
            restaurantViewModel.loadRestaurants()
        }
    }

    private fun observerViewEvents() {
        restaurantViewModel.loadingRestaurantsEvent.observe(this) { isLoading ->
            if (isLoading) {
                binding.fabSort.isVisible = false
                binding.pgLoading.isVisible = true
                binding.pgLoading.show()
            } else {
                binding.pgLoading.isVisible = false
                binding.pgLoading.hide()
                binding.fabSort.isVisible = true
            }
        }
        restaurantViewModel.showRestaurantsEvent.observe(this) {
            restaurantAdapter.setRestaurants(it)
            binding.fabSort.text = it.sortType.name
            binding.fabSort.extend()
        }
        restaurantViewModel.errorGettingRestaurantsEvent.observe(this) {
            Snackbar.make(
                binding.root,
                getString(R.string.restaurant_error),
                Snackbar.LENGTH_SHORT
            ).show()
        }
        restaurantViewModel.showRestaurantNotFoundEvent.observe(this) {
            Snackbar.make(
                binding.root,
                getString(R.string.search_not_found),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }
}
