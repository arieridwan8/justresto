package dev.arie.justresto.restaurant

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import dev.arie.justresto.R
import dev.arie.justresto.databinding.ItemRestaurantBinding
import dev.arie.justresto.model.Restaurant
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.STATUS_CLOSED
import dev.arie.justresto.model.STATUS_OPEN
import dev.arie.justresto.model.STATUS_ORDER_AHEAD
import dev.arie.justresto.model.SortType

/**
 * Created by arieridwan on 18/12/21.
 */
class RestaurantAdapter : RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {
    private var restaurantData: RestaurantData? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RestaurantViewHolder {
        return RestaurantViewHolder(
            ItemRestaurantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: RestaurantViewHolder,
        position: Int
    ) {
        restaurantData?.let { data ->
            holder.bind(data.restaurants[position], data.sortType)
        }
    }

    override fun getItemCount(): Int {
        return restaurantData?.restaurants?.size ?: 0
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setRestaurants(restaurantData: RestaurantData) {
        this.restaurantData = restaurantData
        notifyDataSetChanged()
    }

    class RestaurantViewHolder(
        private val binding: ItemRestaurantBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(restaurant: Restaurant, sortType: SortType) {
            binding.tvName.text = restaurant.name
            binding.tvStatus.text = restaurant.status
            binding.tvSort.text = String.format(
                binding.root.context.getString(R.string.restaurant_sort_value),
                sortType.name,
                restaurant.getSortValue(sortType)
            )
            binding.viewStatus.setBackgroundColor(
                when (restaurant.status) {
                    STATUS_OPEN -> ContextCompat.getColor(binding.root.context, R.color.statusOpen)
                    STATUS_ORDER_AHEAD -> ContextCompat.getColor(
                        binding.root.context,
                        R.color.statusOrderAhead
                    )
                    STATUS_CLOSED -> ContextCompat.getColor(
                        binding.root.context,
                        R.color.statusClosed
                    )
                    else -> ContextCompat.getColor(binding.root.context, R.color.statusOpen)
                }
            )
        }
    }
}
