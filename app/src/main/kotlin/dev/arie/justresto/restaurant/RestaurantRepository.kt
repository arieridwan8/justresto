package dev.arie.justresto.restaurant

import com.google.gson.Gson
import dev.arie.justresto.R
import dev.arie.justresto.data.JsonParser
import dev.arie.justresto.data.PreferenceDataStoreManager
import dev.arie.justresto.model.RestaurantData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * Created by arieridwan on 17/12/21.
 */
interface RestaurantRepository {
    fun getRestaurants(): Flow<RestaurantData>
}

class RestaurantRepositoryImpl @Inject constructor(
    private val gson: Gson,
    private val jsonParser: JsonParser,
    private val dispatcher: CoroutineDispatcher,
    private val preferenceDataStoreManager: PreferenceDataStoreManager
) : RestaurantRepository {

    override fun getRestaurants(): Flow<RestaurantData> {
        return preferenceDataStoreManager.getRestaurants()
            .map { preference ->
                val restaurantsJsonString: String
                if (preference.isNotEmpty()) {
                    restaurantsJsonString = preference
                } else {
                    restaurantsJsonString = readFromRawJson()
                    preferenceDataStoreManager.saveRestaurants(restaurantsJsonString)
                }

                gson.fromJson(restaurantsJsonString, RestaurantData::class.java)
            }.flowOn(dispatcher)
    }

    private fun readFromRawJson(): String {
        return jsonParser.readRawJson(R.raw.just_resto_restaurants)
    }
}
