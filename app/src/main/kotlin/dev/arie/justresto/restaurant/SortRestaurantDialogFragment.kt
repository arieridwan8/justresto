package dev.arie.justresto.restaurant

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import dev.arie.justresto.R
import dev.arie.justresto.databinding.FragmentSortBinding
import dev.arie.justresto.model.SortOrder
import dev.arie.justresto.model.SortType

/**
 * Created by arieridwan on 18/12/21.
 */
const val SEARCH_KEYWORD = "SearchKeyword"

@AndroidEntryPoint
class SortRestaurantDialogFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentSortBinding? = null
    private val binding get() = _binding!!
    private val restaurantViewModel: RestaurantViewModel by activityViewModels()
    private var keyword: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout = bottomSheetDialog.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
            parentLayout?.let { layout ->
                val behaviour = BottomSheetBehavior.from(layout)
                behaviour.isHideable = false
                behaviour.isDraggable = false
                behaviour.state = BottomSheetBehavior.STATE_EXPANDED
                setupFullHeight(layout)
            }
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSortBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        keyword = arguments?.getString(SEARCH_KEYWORD)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupFullHeight(bottomSheet: View) {
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        bottomSheet.layoutParams = layoutParams
    }

    private fun initViews() {
        binding.ivClose.setOnClickListener {
            dismiss()
        }
        binding.rgSortType.setOnCheckedChangeListener { _, id ->
            restaurantViewModel.selectedSortType = when (id) {
                R.id.rbBestMatch -> SortType.BestMatch
                R.id.rbNewest -> SortType.Newest
                R.id.rbRatingAverage -> SortType.RatingAverage
                R.id.rbDistance -> SortType.Distance
                R.id.rbPopularity -> SortType.Popularity
                R.id.rbAverageProductPrice -> SortType.AverageProductPrice
                R.id.rbDeliveryCost -> SortType.DeliveryCost
                R.id.rbMinimumCost -> SortType.MinimumCost
                else -> SortType.Distance
            }
        }
        binding.rgSortOrder.setOnCheckedChangeListener { _, id ->
            restaurantViewModel.selectedSortOrder = when (id) {
                R.id.rbAscending -> SortOrder.Ascending
                R.id.rbDescending -> SortOrder.Descending
                else -> SortOrder.Ascending
            }
        }
        binding.btnApplySort.setOnClickListener {
            val selectedSortType = restaurantViewModel.selectedSortType
            val selectedSortOrder = restaurantViewModel.selectedSortOrder

            if (keyword.isNullOrEmpty().not()) {
                restaurantViewModel.searchRestaurant(keyword!!, selectedSortType, selectedSortOrder)
            } else {
                restaurantViewModel.loadRestaurants(selectedSortType, selectedSortOrder)
            }
            dismiss()
        }

        restaurantViewModel.showRestaurantsEvent.value?.let {
            binding.rgSortType.check(
                when (it.sortType) {
                    SortType.BestMatch -> R.id.rbBestMatch
                    SortType.Newest -> R.id.rbNewest
                    SortType.RatingAverage -> R.id.rbRatingAverage
                    SortType.Distance -> R.id.rbDistance
                    SortType.Popularity -> R.id.rbPopularity
                    SortType.AverageProductPrice -> R.id.rbAverageProductPrice
                    SortType.DeliveryCost -> R.id.rbDeliveryCost
                    SortType.MinimumCost -> R.id.rbMinimumCost
                }
            )
            binding.rgSortOrder.check(
                when (it.sortOrder) {
                    SortOrder.Ascending -> R.id.rbAscending
                    SortOrder.Descending -> R.id.rbDescending
                }
            )
        }
    }
}
