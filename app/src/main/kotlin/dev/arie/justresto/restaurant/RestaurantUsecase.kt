package dev.arie.justresto.restaurant

import dev.arie.justresto.model.SortType
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.Restaurant
import dev.arie.justresto.model.ResultState
import dev.arie.justresto.model.SortOrder
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

/**
 * Created by arieridwan on 17/12/21.
 */
interface RestaurantUsecase {
    fun getRestaurants(
        sortType: SortType = SortType.Distance,
        sortOrder: SortOrder = SortOrder.Ascending
    ): Flow<ResultState<RestaurantData>>

    fun searchRestaurant(
        keyword: String,
        sortType: SortType = SortType.Distance,
        sortOrder: SortOrder = SortOrder.Ascending
    ): Flow<ResultState<RestaurantData>>
}

class RestaurantUsecaseImpl @Inject constructor(
    private val restaurantRepo: RestaurantRepository,
    private val dispatcher: CoroutineDispatcher
) : RestaurantUsecase {

    override fun getRestaurants(
        sortType: SortType,
        sortOrder: SortOrder
    ): Flow<ResultState<RestaurantData>> {
        return restaurantRepo.getRestaurants()
            .toResultState(sortType, sortOrder)
            .flowOn(dispatcher)
    }

    override fun searchRestaurant(
        keyword: String,
        sortType: SortType,
        sortOrder: SortOrder
    ): Flow<ResultState<RestaurantData>> {
        return restaurantRepo.getRestaurants()
            .map { result ->
                result.copy(
                    restaurants = filterRestaurants(
                        result.restaurants,
                        keyword
                    ),
                    sortType = sortType,
                    sortOrder = sortOrder
                )
            }
            .toResultState(sortType, sortOrder)
            .flowOn(dispatcher)
    }

    private fun Flow<RestaurantData>.toResultState(
        sortType: SortType,
        sortOrder: SortOrder
    ) = this
        .map { result ->
            ResultState.Success(
                result.copy(
                    restaurants = sortRestaurants(
                        result.restaurants,
                        sortType,
                        sortOrder
                    ),
                    sortType = sortType,
                    sortOrder = sortOrder
                )
            ) as ResultState<RestaurantData>
        }
        .catch { emit(ResultState.Error(it.message, it)) }
        .onStart { emit(ResultState.Loading) }

    private fun sortRestaurants(
        restaurants: List<Restaurant>,
        sortType: SortType,
        sortOrder: SortOrder
    ) = restaurants.sortedWith(
        if (sortOrder == SortOrder.Ascending) {
            compareBy(
                { it.getSortingPriorityByOpeningHours().toDouble() },
                { it.getSortValue(sortType) }
            )
        } else {
            compareBy<Restaurant> {
                it.getSortingPriorityByOpeningHours().toDouble()
            }.thenByDescending {
                it.getSortValue(sortType)
            }
        }
    )

    private fun filterRestaurants(
        restaurants: List<Restaurant>,
        keyword: String
    ) = restaurants.filter { restaurant ->
        restaurant.name.lowercase().contains(keyword.lowercase())
    }
}
