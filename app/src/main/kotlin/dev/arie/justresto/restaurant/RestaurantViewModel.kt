package dev.arie.justresto.restaurant

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.ResultState
import dev.arie.justresto.model.SortOrder
import dev.arie.justresto.model.SortType
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by arieridwan on 17/12/21.
 */

const val DEBOUNCE_DELAY: Long = 500

@HiltViewModel
class RestaurantViewModel @Inject constructor(
    private val restaurantUsecase: RestaurantUsecase
) : ViewModel() {

    private val _showRestaurantsEvent: MutableLiveData<RestaurantData> = MutableLiveData()
    val showRestaurantsEvent: MutableLiveData<RestaurantData>
        get() = _showRestaurantsEvent

    private val _showRestaurantNotFoundEvent: MutableLiveData<Boolean> = MutableLiveData()
    val showRestaurantNotFoundEvent: MutableLiveData<Boolean>
        get() = _showRestaurantNotFoundEvent

    private val _errorGettingRestaurantsEvent:
            MutableLiveData<ResultState.Error<RestaurantData>> = MutableLiveData()
    val errorGettingRestaurantsEvent: MutableLiveData<ResultState.Error<RestaurantData>>
        get() = _errorGettingRestaurantsEvent

    private val _loadingRestaurantsEvent: MutableLiveData<Boolean> = MutableLiveData()
    val loadingRestaurantsEvent: MutableLiveData<Boolean>
        get() = _loadingRestaurantsEvent

    var selectedSortType = SortType.Distance
    var selectedSortOrder = SortOrder.Ascending
    private var searchJob: Job? = null

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }

    fun loadRestaurants(
        sortType: SortType = selectedSortType,
        sortOrder: SortOrder = selectedSortOrder
    ) {
        viewModelScope.launch {
            restaurantUsecase.getRestaurants(sortType, sortOrder)
                .collect { state -> handleResultState(state) }
        }
    }

    fun searchRestaurant(
        keyword: String,
        sortType: SortType = selectedSortType,
        sortOrder: SortOrder = selectedSortOrder
    ) {
        viewModelScope.launch {
            restaurantUsecase.searchRestaurant(keyword, sortType, sortOrder)
                .collect { state -> handleResultState(state) }
        }
    }

    fun searchRestaurantOnTyping(
        keyword: String,
        sortType: SortType = selectedSortType,
        sortOrder: SortOrder = selectedSortOrder
    ) {
        searchJob?.cancel()

        if (keyword.isEmpty()) {
            loadRestaurants()
            return
        }

        searchJob = viewModelScope.launch {
            delay(DEBOUNCE_DELAY)
            restaurantUsecase.searchRestaurant(keyword, sortType, sortOrder)
                .collect { state -> handleResultState(state) }
        }
    }

    private fun handleResultState(state: ResultState<RestaurantData>) {
        when (state) {
            is ResultState.Loading -> _loadingRestaurantsEvent.value = true
            is ResultState.Success -> {
                _loadingRestaurantsEvent.value = false
                if(state.data.restaurants.isNotEmpty()) {
                    _showRestaurantsEvent.value = state.data
                } else {
                    _showRestaurantNotFoundEvent.value = true
                }
            }
            is ResultState.Error -> {
                _loadingRestaurantsEvent.value = false
                _errorGettingRestaurantsEvent.value = state
            }
        }
    }
}
