package dev.arie.justresto

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasEntry
import org.hamcrest.Matchers.instanceOf

/**
 * Created by arieridwan on 18/12/21.
 */
open class BaseTestRobot {

    fun click(resId: Int): ViewInteraction =
        onView((withId(resId))).perform(ViewActions.click())

    fun matchItemText(
        resId: Int,
        position: Int,
        itemText: String
    ): ViewInteraction {
        scrollItem(resId, position)
        return onView(withText(itemText)).check(matches(isDisplayed()))
    }

    fun scrollItem(resId: Int, position: Int): ViewInteraction =
        onView(withId(resId))
            .perform(
                RecyclerViewActions
                    .scrollToPosition<RecyclerView.ViewHolder>(position)
            )

    fun fillEditText(resId: Int, text: String): ViewInteraction =
        onView(withId(resId)).perform(ViewActions.replaceText(text), ViewActions.closeSoftKeyboard())
}
