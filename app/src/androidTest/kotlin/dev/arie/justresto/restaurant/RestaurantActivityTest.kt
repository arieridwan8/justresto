package dev.arie.justresto.restaurant

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import dev.arie.justresto.BaseTestRobot
import dev.arie.justresto.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@LargeTest
class RestaurantActivityTest: BaseTestRobot() {

    @get:Rule
    val activityRule = ActivityScenarioRule(RestaurantActivity::class.java)

    @Test
    fun verifyFirstAndLastItemWithDefaultSorting() {
        val firstItemPosition = 0
        val expectedFirstItemName = "Tanoshii Sushi"
        val lastItemPosition = 18
        val expectedLastItemName = "Zenzai Sushi"

        restaurantScreen {
            Thread.sleep(200)
            matchItemText(
                R.id.rvRestaurant,
                firstItemPosition,
                expectedFirstItemName
            )

            matchItemText(
                R.id.rvRestaurant,
                lastItemPosition,
                expectedLastItemName
            )
        }
    }

    @Test
    fun verifyFirstAndLastItemWithSelectedSorting() {
        val firstItemPosition = 0
        val lastItemPosition = 18
        val expectedFirstItemName = "Tanoshii Sushi"
        val expectedLastItemName = "Zenzai Sushi"
        val expectedFirstItemNameAfterSorted = "Lunchpakketdienst"
        val expectedLastItemNameAfterSorted = "Tandoori Express"

        restaurantScreen {
            Thread.sleep(200)
            matchItemText(
                R.id.rvRestaurant,
                firstItemPosition,
                expectedFirstItemName
            )
            matchItemText(
                R.id.rvRestaurant,
                lastItemPosition,
                expectedLastItemName
            )

            click(R.id.fabSort)

            click(R.id.rbBestMatch)
            click(R.id.rbDescending)
            click(R.id.btnApplySort)

            Thread.sleep(200)
            matchItemText(
                R.id.rvRestaurant,
                firstItemPosition,
                expectedFirstItemNameAfterSorted
            )
            matchItemText(
                R.id.rvRestaurant,
                lastItemPosition,
                expectedLastItemNameAfterSorted
            )
        }
    }

    @Test
    fun verifySearchRestaurantName() {
        val firstItemPosition = 0
        val expectedFirstItemName = "Tanoshii Sushi"
        val expectedFirstItemNameAfterSearch = "Tandoori Express"

        Thread.sleep(200)
        matchItemText(
            R.id.rvRestaurant,
            firstItemPosition,
            expectedFirstItemName
        )

        click(R.id.etSearch)
        fillEditText(R.id.etSearch, "tand")
        Thread.sleep(200)
        matchItemText(
            R.id.rvRestaurant,
            firstItemPosition,
            expectedFirstItemNameAfterSearch
        )

        click(R.id.ivClear)
        Thread.sleep(200)
        matchItemText(
            R.id.rvRestaurant,
            firstItemPosition,
            expectedFirstItemName
        )
    }

    @Test
    fun verifySearchAndSorting() {
        val firstItemPosition = 0
        val expectedFirstItemNameAfterSearch = "Tanoshii Sushi"
        val expectedFirstItemNameAfterSorting = "Sushi One"

        click(R.id.etSearch)
        fillEditText(R.id.etSearch, "sushi")
        Thread.sleep(200)
        matchItemText(
            R.id.rvRestaurant,
            firstItemPosition,
            expectedFirstItemNameAfterSearch
        )

        click(R.id.fabSort)
        click(R.id.rbBestMatch)
        click(R.id.rbDescending)
        click(R.id.btnApplySort)

        Thread.sleep(200)
        matchItemText(
            R.id.rvRestaurant,
            firstItemPosition,
            expectedFirstItemNameAfterSorting
        )
    }

    private fun restaurantScreen(func: BaseTestRobot.() -> Unit) = BaseTestRobot()
        .apply { func() }
}
