package dev.arie.justresto.data

import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import dev.arie.justresto.R
import dev.arie.justresto.model.RestaurantData
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class JsonParserTest {

    private val gson = Gson()
    private lateinit var jsonParser: JsonParser

    @Before
    fun setUp() {
        jsonParser = JsonParser(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @Test
    fun shouldSuccessfullyReadRawJson() {
        val jsonString = jsonParser.readRawJson(R.raw.just_resto_restaurants)
        val json = gson.fromJson(jsonString, RestaurantData::class.java)
        assertEquals("1", json.restaurants[0].id)
    }
}
