package dev.arie.justresto.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import app.cash.turbine.test
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File

private const val TEST_PREFERENCE_FILE = "test-preferences-file"

@ExperimentalCoroutinesApi
class PreferenceDataStoreManagerTest {

    private val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()
    private lateinit var dataStore: DataStore<Preferences>
    private lateinit var preferenceDataStoreManager: PreferenceDataStoreManager

    @Before
    fun setUp() {
        dataStore = PreferenceDataStoreFactory.create(scope = CoroutineScope(testDispatcher)) {
            InstrumentationRegistry.getInstrumentation().targetContext.preferencesDataStoreFile(
                TEST_PREFERENCE_FILE
            )
        }
        preferenceDataStoreManager = PreferenceDataStoreManager(dataStore)
    }

    @After
    fun tearDown() {
        File(
            ApplicationProvider.getApplicationContext<Context>().filesDir,
            "datastore"
        ).deleteRecursively()
    }

    @Test
    fun shouldSuccessfullySavedRestaurantsData() {
        val restaurants = "justPretendThisIsAJsonString"
        testDispatcher.runBlockingTest {
            preferenceDataStoreManager.saveRestaurants(restaurants)

            preferenceDataStoreManager.getRestaurants().test {
                assertEquals(restaurants, awaitItem())
            }
        }
    }
}
