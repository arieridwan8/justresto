package dev.arie.justresto.restaurant

import app.cash.turbine.test
import com.google.gson.Gson
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.ResultState
import dev.arie.justresto.model.SortOrder
import dev.arie.justresto.model.SortType
import dev.arie.justresto.util.readRestaurantFromRawJson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class RestaurantUsecaseImplTest {

    private val repo = mock<RestaurantRepository>()
    private val dispatcher = TestCoroutineDispatcher()
    private val usecase: RestaurantUsecase = RestaurantUsecaseImpl(repo, dispatcher)
    private val jsonString = javaClass.classLoader?.readRestaurantFromRawJson()
    private val restaurants = Gson().fromJson(jsonString, RestaurantData::class.java)

    @Test
    fun `should return success sorted just by opening hours in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }
            usecase.getRestaurants().test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "1", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                /**
                 * SortType.Distance && SortOrder.Ascending are default sorting
                 * configuration
                 */
                assertTrue(
                   item.sortType == SortType.Distance
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by best match in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.BestMatch).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "1", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.BestMatch
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by newest in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Newest).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "1", item.restaurants.first().id
                )
                assertEquals(
                    "2", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Newest
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by rating average in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.RatingAverage).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "17", item.restaurants.first().id
                )
                assertEquals(
                    "2", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.RatingAverage
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by distance in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Distance).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "1", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Distance
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by popularity in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Popularity).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "17", item.restaurants.first().id
                )
                assertEquals(
                    "2", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Popularity
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by average product price in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.AverageProductPrice).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "17", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.AverageProductPrice
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by delivery cost in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.DeliveryCost).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "4", item.restaurants.first().id
                )
                assertEquals(
                    "10", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.DeliveryCost
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by minimum cost in ascending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.MinimumCost).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "17", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.MinimumCost
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by best match in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.BestMatch, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "19", item.restaurants.first().id
                )
                assertEquals(
                    "2", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.BestMatch
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by newest in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Newest, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "12", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Newest
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by rating average in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.RatingAverage, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "1", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.RatingAverage
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by distance in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Distance, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "19", item.restaurants.first().id
                )
                assertEquals(
                    "10", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Distance
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by popularity in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.Popularity, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "5", item.restaurants.first().id
                )
                assertEquals(
                    "10", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.Popularity
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by average product price in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.AverageProductPrice, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "19", item.restaurants.first().id
                )
                assertEquals(
                    "11", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.AverageProductPrice
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by delivery cost in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.DeliveryCost, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "19", item.restaurants.first().id
                )
                assertEquals(
                    "14", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.DeliveryCost
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return success sorted by minimum cost in descending order`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            usecase.getRestaurants(SortType.MinimumCost, SortOrder.Descending).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(
                    "19", item.restaurants.first().id
                )
                assertEquals(
                    "10", item.restaurants.last().id
                )
                assertTrue(
                    item.sortType == SortType.MinimumCost
                )
                assertTrue(
                    item.sortOrder == SortOrder.Descending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return error when failed to get restaurants`() {
        dispatcher.runBlockingTest {
            val throwable = Throwable("RandomErrorOnUsecase")
            whenever(repo.getRestaurants()) doReturn flow {
                throw throwable
            }

            usecase.getRestaurants().test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Error)
                assertEquals(throwable.message, item.message)

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return result if keyword is found`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            val keyword = "Tanoshi"
            usecase.searchRestaurant(keyword).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertTrue(
                    item.restaurants.first().name.contains(keyword)
                )
                assertTrue(
                    item.sortType == SortType.Distance
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return result if keyword is NOT found`() {
        dispatcher.runBlockingTest {
            whenever(repo.getRestaurants()) doReturn flow {
                emit(restaurants)
            }

            val keyword = "zeezee"
            usecase.searchRestaurant(keyword).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Success).data
                assertEquals(0, item.restaurants.size)
                assertTrue(
                    item.sortType == SortType.Distance
                )
                assertTrue(
                    item.sortOrder == SortOrder.Ascending
                )

                awaitComplete()
            }
        }
    }

    @Test
    fun `should return error when failed to search restaurant`() {
        dispatcher.runBlockingTest {
            val throwable = Throwable("RandomErrorOnUsecase")
            whenever(repo.getRestaurants()) doReturn flow {
                throw throwable
            }

            val keyword = "tanoshi"
            usecase.searchRestaurant(
                keyword
            ).test {
                assertTrue(awaitItem() is ResultState.Loading)

                val item = (awaitItem() as ResultState.Error)
                assertEquals(throwable.message, item.message)

                awaitComplete()
            }
        }
    }
}
