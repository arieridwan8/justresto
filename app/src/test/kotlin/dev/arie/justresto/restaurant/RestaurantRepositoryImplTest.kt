package dev.arie.justresto.restaurant

import app.cash.turbine.test
import com.google.gson.Gson
import dev.arie.justresto.data.JsonParser
import dev.arie.justresto.data.PreferenceDataStoreManager
import dev.arie.justresto.util.getRestaurantsJsonString
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doThrow
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import kotlin.NullPointerException

@ExperimentalCoroutinesApi
class RestaurantRepositoryImplTest {

    private val gson = Gson()
    private val jsonParser = mock<JsonParser>()
    private val dispatcher = TestCoroutineDispatcher()
    private val preferenceDataStoreManager = mock<PreferenceDataStoreManager>()

    private val repo: RestaurantRepository = RestaurantRepositoryImpl(
        gson, jsonParser, dispatcher, preferenceDataStoreManager
    )

    @Test
    fun `should return result from cache when cache is available`() {
        dispatcher.runBlockingTest {
            whenever(preferenceDataStoreManager.getRestaurants()) doReturn flow {
                emit(getRestaurantsJsonString())
            }

            repo.getRestaurants().test {
                assertTrue(awaitItem().restaurants.isNotEmpty())
                awaitComplete()
            }
        }
    }

    @Test
    fun `should return result from raw json when cache is NOT available`() {
        dispatcher.runBlockingTest {
            whenever(preferenceDataStoreManager.getRestaurants()) doReturn flow {
                emit("")
            }
            whenever(jsonParser.readRawJson(any())) doReturn(getRestaurantsJsonString())

            repo.getRestaurants().test {
                assertTrue(awaitItem().restaurants.isNotEmpty())
                awaitComplete()
            }
        }
    }

    @Test
    fun `should return error when failed to fetch result from cache`() {
        dispatcher.runBlockingTest {
            val expectedError = Throwable("RandomErrorInPreference")
            whenever(preferenceDataStoreManager.getRestaurants()) doReturn flow {
                throw expectedError
            }

            repo.getRestaurants().test {
                assertEquals(expectedError.message, awaitError().message)
            }
        }
    }

    @Test
    fun `should return error when failed to fetch result from raw json`() {
        dispatcher.runBlockingTest {
            whenever(preferenceDataStoreManager.getRestaurants()) doReturn flow {
                emit("")
            }
            whenever(jsonParser.readRawJson(any())) doThrow(NullPointerException::class)

            repo.getRestaurants().test {
                awaitError()
            }
        }
    }
}
