package dev.arie.justresto.restaurant

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.ResultState
import dev.arie.justresto.model.SortOrder
import dev.arie.justresto.model.SortType
import dev.arie.justresto.util.CoroutineTestRule
import dev.arie.justresto.util.emptyRestaurantData
import dev.arie.justresto.util.getRestaurantData
import dev.arie.justresto.util.readRestaurantFromRawJson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class RestaurantViewModelTest {

    private val dispatcher = TestCoroutineDispatcher()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testRule = CoroutineTestRule(dispatcher)

    private val usecase = mock<RestaurantUsecase>()
    private val viewModel = RestaurantViewModel(usecase)
    private val jsonString = javaClass.classLoader?.readRestaurantFromRawJson()
    private val restaurants = Gson().fromJson(jsonString, RestaurantData::class.java)

    @Test
    fun `should trigger loading event when loading restaurants`() {
        whenever(
            usecase.getRestaurants(
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Loading)
        }

        viewModel.loadRestaurants(SortType.Distance, SortOrder.Ascending)

        assertTrue(viewModel.loadingRestaurantsEvent.value ?: false)
    }

    @Test
    fun `should trigger show restaurants event when loading restaurants`() {
        val expectedRestaurantName = "Tanoshii Sushi"
        whenever(
            usecase.getRestaurants(
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Success(restaurants))
        }

        viewModel.loadRestaurants(SortType.Distance, SortOrder.Ascending)

        assertEquals(
            expectedRestaurantName,
            viewModel.showRestaurantsEvent.value?.restaurants?.firstOrNull()?.name
        )
    }

    @Test
    fun `should trigger error event when loading restaurants`() {
        val throwable = Throwable("JustRandomError")
        whenever(
            usecase.getRestaurants(
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Error<RestaurantData>(throwable.message, throwable))
        }

        viewModel.loadRestaurants(SortType.Distance, SortOrder.Ascending)

        assertEquals(throwable.message, viewModel.errorGettingRestaurantsEvent.value?.message)
    }

    @Test
    fun `should trigger loading event when search restaurant`() {
        val keyword = "tanoshi"
        whenever(
            usecase.searchRestaurant(
                keyword,
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Loading)
        }

        viewModel.searchRestaurant(keyword, SortType.Distance, SortOrder.Ascending)

        assertTrue(viewModel.loadingRestaurantsEvent.value ?: false)
    }

    @Test
    fun `should trigger show restaurants event when search restaurant`() {
        val keyword = "tand"
        val expectedRestaurantName = "Tandoori Express"
        val fakeSearchResult = getRestaurantData()
        whenever(
            usecase.searchRestaurant(
                keyword,
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Success(fakeSearchResult))
        }

        viewModel.searchRestaurant(keyword, SortType.Distance, SortOrder.Ascending)

        assertEquals(
            expectedRestaurantName,
            viewModel.showRestaurantsEvent.value?.restaurants?.firstOrNull()?.name
        )
    }

    @Test
    fun `should trigger search not found event`() {
        val keyword = "zeezee"
        val fakeSearchResult = emptyRestaurantData()
        whenever(
            usecase.searchRestaurant(
                keyword,
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Success(fakeSearchResult))
        }

        viewModel.searchRestaurant(keyword, SortType.Distance, SortOrder.Ascending)

        assertTrue(viewModel.showRestaurantNotFoundEvent.value ?: false)
    }

    @Test
    fun `should trigger error event when search restaurant`() {
        val keyword = "tanoshi"
        val throwable = Throwable("JustRandomError")
        whenever(
            usecase.searchRestaurant(
                keyword,
                SortType.Distance,
                SortOrder.Ascending
            )
        ) doReturn flow {
            emit(ResultState.Error<RestaurantData>(throwable.message, throwable))
        }

        viewModel.searchRestaurant(keyword, SortType.Distance, SortOrder.Ascending)

        assertEquals(throwable.message, viewModel.errorGettingRestaurantsEvent.value?.message)
    }

    @Test
    fun `should trigger show restaurants event when search restaurant on text changed`() {
        dispatcher.runBlockingTest {
            val keyword = "tan"
            val expectedRestaurantName = "Tandoori Express"
            val fakeSearchResult = getRestaurantData()
            whenever(
                usecase.searchRestaurant(
                    keyword,
                    SortType.Distance,
                    SortOrder.Ascending
                )
            ) doReturn flow {
                emit(ResultState.Success(fakeSearchResult))
            }

            viewModel.searchRestaurantOnTyping(keyword, SortType.Distance, SortOrder.Ascending)

            advanceTimeBy(500)
            assertEquals(
                expectedRestaurantName,
                viewModel.showRestaurantsEvent.value?.restaurants?.firstOrNull()?.name
            )
        }
    }
}
