package dev.arie.justresto.util

import dev.arie.justresto.data.BUFFER
import dev.arie.justresto.data.CHARSET_NAME
import dev.arie.justresto.model.Restaurant
import dev.arie.justresto.model.RestaurantData
import dev.arie.justresto.model.SortingValues
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.io.StringWriter
import java.io.Writer

fun getRestaurantsJsonString() =
    "{\n" +
            "  \"restaurants\": [\n" +
            "    {\n" +
            "      \"id\": \"1\",\n" +
            "      \"name\": \"Tanoshii Sushi\",\n" +
            "      \"status\": \"open\",\n" +
            "      \"sortingValues\": {\n" +
            "        \"bestMatch\": 0.0,\n" +
            "        \"newest\": 96.0,\n" +
            "        \"ratingAverage\": 4.5,\n" +
            "        \"distance\": 1190,\n" +
            "        \"popularity\": 17.0,\n" +
            "        \"averageProductPrice\": 1536,\n" +
            "        \"deliveryCosts\": 200,\n" +
            "        \"minCost\": 1000\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n"

fun getRestaurantData() =
    RestaurantData(
        restaurants = listOf(
            Restaurant(
                id = "2",
                name = "Tandoori Express",
                status = "closed",
                sortingValues = SortingValues(
                    bestMatch = 1.0,
                    newest = 266.0,
                    ratingAverage = 4.5,
                    distance = 2308,
                    popularity = 123.0,
                    averageProductPrice = 1146,
                    deliveryCosts = 150,
                    minCost = 1300
                )
            )
        )
    )

fun emptyRestaurantData() = RestaurantData(restaurants = listOf())

fun ClassLoader?.readRestaurantFromRawJson(): String {
    val inputStream = this?.getResourceAsStream("just_resto_restaurants.json")
    val writer: Writer = StringWriter()
    val buffer = CharArray(BUFFER)

    inputStream.use { input ->
        val reader: Reader = BufferedReader(InputStreamReader(input, CHARSET_NAME))
        var counter: Int
        while (reader.read(buffer).also { counter = it } != -1) {
            writer.write(buffer, 0, counter)
        }
    }

    return writer.toString()
}
