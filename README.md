# Just Resto
Simple app to demonstrate parsing JSON and sorting capability.

1. Opening state: Restaurant is either open (top), you can order ahead (middle) or a restaurant is currently closed (bottom). (Values available in [just_resto_restaurants.json](app/src/main/res/raw/just_resto_restaurants.json))
2. Sort options: Always one sort option is chosen and this can be best match, newest, rating average, distance, popularity, average product price, delivery costs or the minimum cost. (Values available in [just_resto_restaurants.json](app/src/main/res/raw/just_resto_restaurants.json))

### Checkpoints
- [x] Init repository
- [x] Migrate gradle to use KTS
- [x] Setup basic dependency
- [x] Setup dependency injection
- [x] Implement data layer
- [x] Implement domain layer
- [x] Implement presentation layer

### How to run app
```shell
./gradlew app:installDebug
```

### How to run unit test
```shell
./gradlew app:test
```

### How to run UI test
```shell
./gradlew app:connectedAndroidTest
```

### Notes
If UI test is keep returning error, try to run it from IDE.
