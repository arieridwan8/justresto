object Dependency {
    object Versions {
        const val kotlin = "1.5.10"
        const val agp = "7.0.0"
        const val junit4 = "4.13.2"
        const val mockito = "4.0.0"
        const val hilt = "2.38.1"
        const val lifecycle = "2.3.0"
        const val coroutines = "1.5.2"
        const val espresso = "3.4.0"
    }

    object Android {
        const val material = "com.google.android.material:material:1.4.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.1.2"
        const val coreKtx = "androidx.core:core-ktx:1.6.0"
        const val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
        const val hiltCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
        const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
        const val activityKtx = "androidx.activity:activity-ktx:1.3.0"
        const val fragmentKtx = "androidx.fragment:fragment-ktx:1.3.0"
        const val dataStore = "androidx.datastore:datastore-preferences:1.0.0"
        const val gson = "com.google.code.gson:gson:2.8.9"

        object Build {
            const val appId = "dev.arie.justresto"
            const val compileSdkVersion = 30
            const val minSdkVersion = 21
            const val targetSdkVersion = 30
            const val versionCode = 1
            const val versionName = "1.0.0"
            const val runner = "androidx.test.runner.AndroidJUnitRunner"
        }

        object Plugin {
            const val agp = "com.android.tools.build:gradle:${Versions.agp}"
            const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
            const val hilt = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"
        }
    }

    object Kotlin {
        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"

        object Coroutines {
            const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
            const val android =
                "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
        }
    }

    object Test {
        const val jUnit4 = "junit:junit:${Versions.junit4}"
        const val mockito = "org.mockito.kotlin:mockito-kotlin:${Versions.mockito}"
        const val testRunner = "androidx.test:runner:1.4.0"
        const val testExtension = "androidx.test.ext:junit:1.1.2"
        const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
        const val turbine = "app.cash.turbine:turbine:0.7.0"
        const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}"
        const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
        const val espressoContrib = "androidx.test.espresso:espresso-contrib:${Versions.espresso}"
    }
}
