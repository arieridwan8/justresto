buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath(Dependency.Android.Plugin.agp)
        classpath(Dependency.Android.Plugin.kotlin)
        classpath(Dependency.Android.Plugin.hilt)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
}

val clean by tasks.creating(Delete::class) {
    delete(rootProject.buildDir)
}
